//
//  BusinessLogic.swift
//  calc
//
//  Created by Admin on 27.08.2018.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import Darwin.C.math
import CoreData
import UIKit



/*

var ProcentArray = [Int]()
var kreditCalculator = KreditCalculator()






var creditData = CreditData()

kreditCalculator.prepareCreditInformation(creditData: creditData)

print(FileManager.default.urls(for: .documentDirectory, in: .userDomainMask))


kreditCalculator.CalculateTable(creditData: creditData, recount: false)
tableView.reloadData()


    
creditData = kreditCalculator.dosrhochnoye_pogashenie(creditData:creditData,summa:250000, at:Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2018, month: 08, day: 22))!)
print("$$$$$ INTERVAL")
print(creditData.range)



creditData = kreditCalculator.dosrhochnoye_pogashenie(creditData:creditData,summa:182733.0, at:Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2018, month: 08, day: 27))!)
print("$$$$$ INTERVAL")
print(creditData.range)
creditData = kreditCalculator.dosrhochnoye_pogashenie(creditData:creditData,summa:350000.0, at:Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2018, month: 08, day: 29))!)
print("$$$$$ INTERVAL")
print(creditData.range)
creditData = kreditCalculator.dosrhochnoye_pogashenie(creditData:creditData,summa:10360.0, at:Calendar.current.date(from: DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2018, month: 10, day: 02))!)
print("$$$$$ NOVIE PARAMETRI KREDITA")
print(creditData.range)
print(creditData.paymentDatePoGraphicy)
print(creditData.credit)

kreditCalculator.prepareCreditInformation(creditData: creditData)
kreditCalculator.CalculateTable(creditData: creditData,recount: true)
tableView.reloadData()
//}








let cell = tableView.dequeueReusableCell(withIdentifier: "Item",for:indexPath)

let item = kreditCalculator.sberEntityArray[indexPath.row]



let dateFormatter = DateFormatter()
dateFormatter.dateFormat = "yyyy-MM-dd"
let dateString = dateFormatter.string(from:item.payment_date as! Date)


cell.textLabel?.text = "Дата: " + dateString +
    " | Основной долг: " + String(item.osnovnoy_dolg) +
    " | Проценты: " + String(item.procent) +
    " | Сум оплате: " + String(  item.summ_for_pay) +
    " | Ост осн длга: " + String( item.ostatok_po_dolgy)
return cell
}





class KreditCalculator {
    
    var plata_v_month:Double = 0.0
    var sberEntityArray = [SberEntity]()
  
    var procentnay_stavka_v_month:Double = 0
    var k:Double = 0
    var full_kredit:Double = 0
    var pereplata:Double = 0
    var platesh_po_procentam:Double = 0
    var current_kredit:Double = 0
    var nextDateForPayment:DateComponents = DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2018, month: 09, day: 03)
    var osnovnoy_dolg:Double = 0
    var interval_for_procents:Int = 0
    var procent_stavka_v_den:Double = 0
    init(){
        print("INIT")
        
    }
    
    
    
 
    
    
    fileprivate func putCalculatedItemInCoreData(_ paymentDatePoGraphicy: Date?) {
        let newItem = SberEntity(context: context)
        newItem.payment_date = paymentDatePoGraphicy
       // print("!!!!!!!!!!!!!!!! paymentDatePoGraphicy: \(paymentDatePoGraphicy)")
        newItem.osnovnoy_dolg = Double(osnovnoy_dolg).rounded(toPlaces: 2)
        newItem.procent = Double(platesh_po_procentam).rounded(toPlaces: 2)
        newItem.ostatok_po_dolgy = Double(current_kredit).rounded(toPlaces: 2)
        newItem.summ_for_pay = Double(plata_v_month).rounded(toPlaces: 2)
        sberEntityArray.append(newItem)
       //  print("!!!!!!!!!!!!!!!!222 paymentDatePoGraphicy: \(newItem.payment_date)")
    }
    
    func CalculateTable(creditData:CreditData,recount:Bool) {
        
       // clearTable()
        if(recount == false){
            
            
            clearTable()
        }
        //    print("Fill CoreData Table")
        
        
        
            //TRY TO FIND EXISTING DATE
            print("  creditData.paymentDatePoGraphicy: \(creditData.paymentDatePoGraphicy)")
            
            
            
            //POCHEMUTO BILO + 9
            var test111 = DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: creditData.paymentDatePoGraphicy.year, month: creditData.paymentDatePoGraphicy.month, day: creditData.paymentDatePoGraphicy.day)
            
            
            var myDate = Calendar.current.date(from: test111)
            
            print("myDate")
            print(myDate)
            
            
            let request1: NSFetchRequest<SberEntity> = SberEntity.fetchRequest()
            let sort = NSSortDescriptor(key: #keyPath(SberEntity.payment_date), ascending: true)
            request1.sortDescriptors = [sort]
            let predicate = NSPredicate(format: "payment_date == %@", myDate as! NSDate)
            request1.predicate = predicate
            
            //paymentDatePoGraphicy: Optional(2023-08-03 00:00:00 +0000)
            //Optional(2018-10-01 21:00:00 +0000)
            var myArray = [SberEntity]()
            
            do {
                myArray = try context.fetch(request1)
            }
            catch {
                print("ERROR FETCHING DATA")
            }
            
            
            if(myArray.count != 0){
                print("VALUE FOUND!!!")
                print(myArray[0].osnovnoy_dolg)
                
                if(recount == true){
                clearEntitiesAfterThisDate(date: myArray[0].payment_date!)
                saveCoreData()
                }
            }
            
        
        
        
        
        
        
        
        
        var  paymentDatePoGraphicy = Calendar.current.date(from: creditData.paymentDatePoGraphicy)
        
        for index in 0..<Int(creditData.range){
            
            osnovnoy_dolg = plata_v_month - platesh_po_procentam
            current_kredit -= osnovnoy_dolg
            platesh_po_procentam = current_kredit * procentnay_stavka_v_month
           // print("PPP:")
          //  print(paymentDatePoGraphicy)
            putCalculatedItemInCoreData(paymentDatePoGraphicy)
            
         //   print("!")
          //  print(sberEntityArray[index].ostatok_po_dolgy)
            
            paymentDatePoGraphicy = Calendar.current.date(byAdding: .month, value: 1, to: paymentDatePoGraphicy!)
            
            
            //   print("Month: \(index+1) : \(Double(round(100*osnovnoy_dolg)/100)) :\(Double(round(100*platesh_po_procentam)/100))")
            //    print("Ostatok_dolga: \(Double(round(100*current_kredit)/100))")
            //    print("PLATA V MONTH \(Double(round(100*plata_v_month)/100))")
            
            
            
            saveCoreData()
            
            
        }
        
        
    }
    
    
    fileprivate func sortCoreDateByDate() {
        let request: NSFetchRequest<SberEntity> = SberEntity.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(SberEntity.payment_date), ascending: true)
        request.sortDescriptors = [sort]
        
        do {
            sberEntityArray = try context.fetch(request)
            
        } catch {
            print("error getting data")
        }
    }
    
    fileprivate func createnewItemInCoreData(_ date: Date, _ sum: Double) {
        let newItem = SberEntity(context: context)
        newItem.payment_date = date
        newItem.summ_for_pay = Double(sum).rounded(toPlaces: 2)
        newItem.osnovnoy_dolg = 0
        newItem.ostatok_po_dolgy = -1
        newItem.procent = 0
        
        sberEntityArray.append(newItem)
    }
    
    fileprivate func osobiySLUCHAI(_ sberEntity: SberEntity, _ creditData: CreditData, _ index: Int) {
        print("THIS IS FIRST VALUE IN CORE DATA SO USE OSNOVNOY DOLG AS IN STRUCT")
        
        print(sberEntity.summ_for_pay)
        
        sberEntity.procent = 0
        sberEntity.osnovnoy_dolg = Double(sberEntity.summ_for_pay - sberEntity.procent).rounded(toPlaces: 2)
        print(sberEntity.osnovnoy_dolg)
        sberEntity.ostatok_po_dolgy = Double(creditData.credit - sberEntity.summ_for_pay).rounded(toPlaces: 2)
        
        
        
        if(index < sberEntityArray.count - 1){
            if(sberEntity.osnovnoy_dolg >= sberEntityArray[index+1].osnovnoy_dolg){
                sberEntityArray[index+1].osnovnoy_dolg = 0
                
                sberEntityArray[index+1].ostatok_po_dolgy  = sberEntityArray[index].ostatok_po_dolgy
                
                
                
                
                interval_for_procents = Calendar.current.dateComponents([.day], from: sberEntityArray[index].payment_date!, to: sberEntityArray[index+1].payment_date!).day!
                
                
                
                print("interval_for_procents: \(interval_for_procents)")
                
                
                procent_stavka_v_den = creditData.procent/365/100
                var ostatok:Double = 0
                if(index == 0) {
                    ostatok = creditData.credit
                } else {
                    ostatok = sberEntityArray[index-1].ostatok_po_dolgy
                }
                
                var procents_for_previouse_period =   Double(ostatok * procent_stavka_v_den * Double(interval_for_procents)).rounded(toPlaces: 2)
                
                
                
                print(" procent_stavka_v_den: \(procent_stavka_v_den)")
                print(" procents_for_previouse_period: \(procents_for_previouse_period)")
                sberEntityArray[index+1].procent = procents_for_previouse_period
                sberEntityArray[index+1].summ_for_pay = procents_for_previouse_period
                
                
                
                
            }
            
        }
    }
    
    fileprivate func ifWePayMoreThanOsnovnoyDolg(_ index: Int, _ sberEntity: SberEntity,creditData:CreditData) {
        // Меняем количество денег которые надо оплатить по графику
       print("TRY 1")
        if(index < sberEntityArray.count - 1){
          print("TRY2 ")
            print(" proverka_boshelisumma_osnovnogo_dolga")
           // print(" \(sberEntity.summ_for_pay) >> \(sberEntityArray[index+1].summ_for_pay)")
          //  print("RAZNITSA \(sberEntityArray[index+1].summ_for_pay - sberEntity.summ_for_pay)")
            print("IMPORTANT")
              print("sberEntity.osnovnoy_dolg: \(sberEntity.osnovnoy_dolg)")
              print("sberEntityArray[index+1].osnovnoy_dolg: \(sberEntityArray[index+1].osnovnoy_dolg)")
            if(sberEntity.osnovnoy_dolg >= sberEntityArray[index+1].osnovnoy_dolg){
                print(" BOLSHE")
                sberEntityArray[index+1].osnovnoy_dolg = 0
                
                sberEntityArray[index+1].ostatok_po_dolgy  = sberEntityArray[index].ostatok_po_dolgy
                print(" noviy ostatok dolga \(sberEntityArray[index+1].ostatok_po_dolgy)")
                
                
                
                var rangeForProcentPay = Calendar.current.dateComponents([.day], from: sberEntityArray[index].payment_date!, to: sberEntityArray[index+1].payment_date!)
                
                var rangeInt = rangeForProcentPay.day
                rangeInt  = rangeInt!
                print(" Interval: \(rangeInt)")
                
                
                //  var procent_stavka_v_den = creditData.procent/365/100
                var ostatok:Double = 0
                if(index == 0) {
                    ostatok = creditData.credit
                } else {
                    ostatok = sberEntityArray[index].ostatok_po_dolgy
                }
                print(" ostatok: \(ostatok)")
                var procents_for_previouse_period =   Double(ostatok * procent_stavka_v_den * Double(rangeInt!)).rounded(toPlaces: 2)
                
                
                
                print(" procent_stavka_v_den: \(procent_stavka_v_den)")
                print(" procents_for_previouse_period: \(procents_for_previouse_period)")
                sberEntityArray[index+1].procent = procents_for_previouse_period
                sberEntityArray[index+1].summ_for_pay = procents_for_previouse_period
                
                
                
                
            } else {
                
                
                //NUSNO ISPOLZOVAT TEKUSHIY PO GRAPHICY DOLG
                  print("NE BOLSHE!!!")
                
                 sberEntityArray[index+1].osnovnoy_dolg = 0
                
            }
            
        }
    }
    
    fileprivate func calculateDosrochniyPlatesh(_ index: Int, _ sberEntity: SberEntity,creditData:CreditData) {
        let sberEntityPreviousValueInCoreData = sberEntityArray[index-1]
        print("Ostatok po dolgy:\(sberEntityPreviousValueInCoreData.ostatok_po_dolgy)")
        
        
        let currentPaymentDate = sberEntity.payment_date
        print("currentPaymentDate: \(currentPaymentDate)")
        
        
        //  print("PreviousePay: \(firstPay)")
        var rangeForProcentPay = Calendar.current.dateComponents([.day], from: sberEntityPreviousValueInCoreData.payment_date!, to: currentPaymentDate!)
        
        var rangeInt = rangeForProcentPay.day
        rangeInt  = rangeInt!
        print(" Interval: \(rangeInt)")
        print(" sberEntityPreviousValueInCoreData.ostatok_po_dolgy!!!: \(sberEntityPreviousValueInCoreData.ostatok_po_dolgy)")
        
        procent_stavka_v_den = creditData.procent/365/100
        var procents_for_previouse_period =   Double(sberEntityPreviousValueInCoreData.ostatok_po_dolgy * procent_stavka_v_den * Double(rangeInt!)).rounded(toPlaces: 2)
        print(" procent_stavka_v_den: \(procent_stavka_v_den)")
        print(" procents_for_previouse_period: \(procents_for_previouse_period)")
        print(" sberEntityPreviousValueInCoreData.ostatok_po_dolgy: \(sberEntityPreviousValueInCoreData.ostatok_po_dolgy)")
        
        
        var procents_for_previouse_period1 =   Double(sberEntityPreviousValueInCoreData.ostatok_po_dolgy * procent_stavka_v_den * 2).rounded(toPlaces: 2)
        print(" procents_for_previouse_period1: \(procents_for_previouse_period1)")
        
        
        sberEntity.osnovnoy_dolg = Double(sberEntity.summ_for_pay - procents_for_previouse_period).rounded(toPlaces: 2)
        sberEntity.procent = procents_for_previouse_period
        
        
        
        
        
        sberEntity.ostatok_po_dolgy = Double(sberEntityPreviousValueInCoreData.ostatok_po_dolgy - sberEntity.osnovnoy_dolg).rounded(toPlaces: 2)
    }
    
    func dosrhochnoye_pogashenie(creditData:CreditData,summa sum:Double, at date:Date)
        -> CreditData
    {
        
        
        if(sum == 10360.0){
            print("HERE")
            
        }
        print("Dosrochnoey pogashenie: \(creditData.paymentDatePoGraphicy) : \(sum) : \(date)")
        
        createnewItemInCoreData(date, sum)
        
        saveCoreData()
        sortCoreDateByDate()
        
        
        
        for (index,sberEntity) in sberEntityArray.enumerated() {
            
            if(sberEntity.ostatok_po_dolgy == -1){
                
                print(sberEntity.ostatok_po_dolgy)
                //FOUND ADDED VALUE IN SORT ORDER
                print(index)
                print(sberEntity.summ_for_pay)
                
                if(index > 0){
                    calculateDosrochniyPlatesh(index, sberEntity, creditData: creditData)
                    
                    
                    
                    ifWePayMoreThanOsnovnoyDolg(index, sberEntity, creditData: creditData)
                    
                    
                    
                    
                } else {
                    
                    osobiySLUCHAI(sberEntity, creditData, index)
                    
                }
                
                var newCreditData = CreditData()
                newCreditData.credit = sberEntity.ostatok_po_dolgy
                
                newCreditData.paymentDatePoGraphicy =   Calendar.current.dateComponents([.year, .month, .day], from: sberEntityArray[index+1].payment_date!)
                
                print("New credit data next payment date \(newCreditData.paymentDatePoGraphicy)")
                
                print("New credit data next payment old \(creditData.dataVidachiKredita)")
                
                
                var newRangeForPay = Calendar.current.dateComponents([.month], from: creditData.dataVidachiKredita, to: newCreditData.paymentDatePoGraphicy)
                print("00! \(newRangeForPay)")
                
                var newRangeForPayInt = newRangeForPay.month
                print("!!!!!!")
                print(newRangeForPayInt)
                creditData.range
                newCreditData.range = Double(Int(creditData.range) - newRangeForPayInt!)
                
                
                return newCreditData
                
            }
            
        }
        
        
        
        return creditData
        
        
        
    }
    
    
    
    func saveCoreData(){
        do{
            try context.save()
        }catch{
            print("Error Saving context")
        }
        
        
    }


func clearEntitiesAfterThisDate(date:Date){
   print(sberEntityArray.count)
    sberEntityArray.removeAll(where:{$0.payment_date! >= date})
      print(sberEntityArray.count)
    
    
 
 
    saveCoreData()
    
    
  
}

    
 



*/
