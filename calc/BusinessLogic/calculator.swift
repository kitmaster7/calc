//
//  calculator.swift
//  calc
//
//  Created by alex on 16.01.2019.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class Calculator{
    
    private lazy var updatedAtDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter
    }()
    
    let dateFormat = "yyyy-MM-dd"
    
    private let creditData:CreditData?
    private let coreDataManager:CoreDataManager
    private var parametriKredita = ParametriKredita()
    
    
    
    //var procentnay_stavka_v_month:Double = 0
    
    /*
     
     var full_kredit:Double = 0
     var pereplata:Double = 0
     var platesh_po_procentam:Double = 0
     var current_kredit:Double = 0
     //var nextDateForPayment:DateComponents = DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: 2018, month: 09, day: 03)
     var osnovnoy_dolg:Double = 0
     var interval_for_procents:Int = 0
     var procent_stavka_v_den:Double = 0
     */
    
    
    init(creditData:CreditData,coreDataManager:CoreDataManager) {
        self.creditData = creditData
        self.coreDataManager = coreDataManager
    }
    
    
    
    func checkFirstPlatesh(creditData:CreditData){
        
        //check if data pervogo platesha menshe po graphicy
        
        var data1 = Calendar.current.date(from: creditData.dataVidachiKredita)
        var data2 = Calendar.current.date(from: creditData.dataPlatesha)
        
        
        if(data1! != data2!){
            print("data nachala kredita ranche dati platesha po graphicy")
            var diffInDays = Calendar.current.dateComponents([.day], from: data1!, to: data2!).day
            print(diffInDays)
            
            
            let interval = Calendar.current.dateInterval(of: .year, for: data1!)!
            print(interval)
            // Compute difference in days:
            let days = Calendar.current.dateComponents([.day], from: interval.start, to: interval.end).day!
            print("NUMBER IN YEAR")
            print(days)
            
            print("DIFF IN DAYS")
            print(diffInDays)
            
            print( Double(diffInDays!) / Double(days))
            
            
            var firstPlatesh = creditData.credit * Double(diffInDays!) / Double(days) * creditData.procent / 100
            print("HERE")
            
            firstPlatesh = Double(round(100*firstPlatesh)/100)
            print(firstPlatesh)
            
            
            //zanosim v tablitsy
            
            
            var  paymentDatePoGraphicy = Calendar.current.date(from: creditData.dataPlatesha)
            
            
            
            var test = DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: creditData.dataPlatesha.year, month: creditData.dataPlatesha.month, day: creditData.dataPlatesha.day)
            
            
            var myDate1 = Calendar.current.date(from: test)
            print(myDate1)
            
            paymentDatePoGraphicy = myDate1
            
            
            
       //  addOneCreditStringtoCoreDataContext(dateOplati: paymentDatePoGraphicy!, osnovnoyDolg: 0, procent: firstPlatesh, summ_for_pay: firstPlatesh, ostatok_kredita: creditData.credit)
            
            var creditDataNew = creditData
            print("NEW CREDIT PARAMETERS")
            
            
            creditDataNew.credit = creditData.credit
            
            
            var  nextMonth = data2
            print("BEFORE NEXT MONTH: \(nextMonth)")
            nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: nextMonth!)
            print("NEXT MONTH: \(nextMonth)")
            // creditDataNew.credit = ostatok_kredita
            creditDataNew.dataPlatesha = Calendar.current.dateComponents([.month,.day,.year],from:nextMonth!)
            //////////
            
            var newRangeForPay = Calendar.current.dateComponents([.month], from: creditDataNew.dataVidachiKredita, to: creditDataNew.dataPlatesha)
            print("00! \(newRangeForPay)")
            
            var newRangeForPayInt = newRangeForPay.month
            print("!!!!!!")
            print(newRangeForPayInt)
            
            creditDataNew.range = Double(Int(creditDataNew.range) - newRangeForPayInt!)
            
            //разобраться с этим все зависит от первого платежа, если он не совпадает
            creditDataNew.range+=1
            print("RANGE MY")
            print(creditDataNew.range)
            ////////////////
            
            
            print(creditDataNew.credit)
            print("NOVIY S \(creditDataNew.dataPlatesha)")
            calculateParametriKredita(creditData:creditDataNew)
          
            calculateKredit(creditData: creditDataNew)
           
           
       
            
            
            
            
        }
        
      
    }
    
    
    
    func calculateParametriKredita(creditData:CreditData){
        print("KREDIT \(creditData.credit)")
         print("KREDIT RANGE\(creditData.range)")
       
        
        
        
        
        
        parametriKredita.procentnay_stavka_v_month = creditData.procent / 12 / 100
        print("Procent stavka v month \(parametriKredita.procentnay_stavka_v_month)")
        
        let i1 = (1+parametriKredita.procentnay_stavka_v_month) ^^ creditData.range
        parametriKredita.k = (parametriKredita.procentnay_stavka_v_month * i1) / (i1 - 1)
        
        parametriKredita.plata_v_month = parametriKredita.k * creditData.credit
        
        
        print("PLATA V MONTH \(Double(round(100*parametriKredita.plata_v_month)/100))")
        
        
        parametriKredita.full_kredit = Double(round(100*parametriKredita.plata_v_month*creditData.range)/100)
        
        print("FULL KREDIT: \(parametriKredita.full_kredit)")
        parametriKredita.pereplata = parametriKredita.full_kredit - creditData.credit
        print("Pereplata: \(parametriKredita.pereplata)")
        
        
        parametriKredita.platesh_po_procentam =  creditData.credit * parametriKredita.procentnay_stavka_v_month
        print("PROCENT PO PLATESHY: \(parametriKredita.platesh_po_procentam)")
        
        
        
         parametriKredita.procent_stavka_v_den = creditData.procent/365/100
    }
    
    
    
    
    func calculateKredit(creditData:CreditData){
        
        var ostatok_kredita:Double = 0
        var osnovnoy_dolg:Double = 0
        var oplata_procentov:Double = 0
        var  paymentDatePoGraphicy = Calendar.current.date(from: creditData.dataPlatesha)
        
        
        
        var test = DateComponents(timeZone:TimeZone.init(abbreviation: "UTC"), year: creditData.dataPlatesha.year, month: creditData.dataPlatesha.month, day: creditData.dataPlatesha.day)
        
        
        var myDate1 = Calendar.current.date(from: test)
        print(myDate1)
        
        paymentDatePoGraphicy = myDate1
        
        
        print("PAYMENT PO GR\(paymentDatePoGraphicy)")
        oplata_procentov = parametriKredita.platesh_po_procentam
        ostatok_kredita = creditData.credit
        
        print("PARAMETRI KREDITA")
        print(ostatok_kredita)
        print(oplata_procentov)
        var ostatok_kredita_final_day:Double = 0
        for i in 0..<Int(creditData.range){
            
            osnovnoy_dolg = parametriKredita.plata_v_month - oplata_procentov
            
            if(i == Int(creditData.range - 1)) {
                ostatok_kredita_final_day = ostatok_kredita
            }
            ostatok_kredita -= osnovnoy_dolg
            
            oplata_procentov = ostatok_kredita * parametriKredita.procentnay_stavka_v_month
            
            
            addOneCreditStringtoCoreDataContext(dateOplati: paymentDatePoGraphicy!, osnovnoyDolg: osnovnoy_dolg, procent: oplata_procentov, summ_for_pay: parametriKredita.plata_v_month, ostatok_kredita: ostatok_kredita)
            
            
            
            
            
          
            paymentDatePoGraphicy = Calendar.current.date(byAdding: .month, value: 1, to: paymentDatePoGraphicy!)
        print("PAYMENT DATE PO GRAPHICY \(paymentDatePoGraphicy)")
            print("ostatok_kredita: \(ostatok_kredita)")
        }
        
        
      
       
           coreDataManager.deleteLastStringandAddNewOneForLastMonth()
           //последний процент считается так, уможноам ставку в день  0.00032054794520547944 на количество дней с 3 августа по 22 августа то етсь 19 и умножаем на 5811.72 остаток кредита -  в основной долг заносим всю сумму и получаем сумму к оплате
          var lastDatePoDogovory = Calendar.current.date(from: creditData.dataLastKredita)
            paymentDatePoGraphicy = Calendar.current.date(byAdding: .month, value: -1, to: paymentDatePoGraphicy!)
        var rangeForProcentPay = Calendar.current.dateComponents([.day], from: lastDatePoDogovory!, to: paymentDatePoGraphicy!)
        
        var rangeInt = rangeForProcentPay.day
        rangeInt  = rangeInt!
        print(" Interval: \(rangeInt)")
        
        print("parametriKredita.procent_stavka_v_den \(parametriKredita.procent_stavka_v_den)")
          print("range \(Double(rangeInt!))")
          print("ostatok \(ostatok_kredita)")
        var lastProcent = parametriKredita.procent_stavka_v_den * Double(rangeInt!) * ostatok_kredita_final_day
        addOneCreditStringtoCoreDataContext(dateOplati: lastDatePoDogovory!, osnovnoyDolg: ostatok_kredita_final_day, procent: lastProcent, summ_for_pay: ostatok_kredita_final_day+lastProcent, ostatok_kredita: ostatok_kredita)
   paymentDatePoGraphicy = Calendar.current.date(byAdding: .month, value: 1, to: paymentDatePoGraphicy!)
   
    
    
    }
    
  
    
    
    
    
    
    func dosrhochnoye_pogashenie(creditData:CreditData,amount:Double, at date:Date){
        
        var ostatok_kredita:Double = 0
        var osnovnoy_dolg:Double = 0
        var oplata_procentov:Double = 0
        var interval_for_procents:Int = 0
       print("AMOUNT")
        print(amount)
        if(amount == 1300.0){
            print("HERE")
        }
        var procents_for_previouse_period:Double = 0
        
        
        
        let request: NSFetchRequest<SberEntity> = SberEntity.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(SberEntity.payment_date), ascending: true)
        request.sortDescriptors = [sort]
      
      
        var predicate = NSPredicate(format: "payment_date < %@", date as! NSDate)
        request.predicate = predicate
        
         var fetchedResultsController: NSFetchedResultsController<SberEntity>!
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                                              managedObjectContext: coreDataManager.mainManagedObjectContext,
                                                              sectionNameKeyPath: nil,
                                                              cacheName: nil)
        print("TRY TO FETCH")
        do {
            print("DOSRHOCHNO Data fetched")
            try fetchedResultsController.performFetch()
          
            
        } catch {
            print("Unable to Perform Fetch Request")
            print("\(error), \(error.localizedDescription)")
        }
        print("COUNT")
        print(fetchedResultsController.fetchedObjects?.count)
        
        //добавить проверку если дата такая есть то добавляем досрочный платеж уже к существующей строке!
        if let previouseCreditString = fetchedResultsController?.fetchedObjects?.last {
        print(previouseCreditString.payment_date)
         
            interval_for_procents = Calendar.current.dateComponents([.day], from: previouseCreditString.payment_date!, to:date).day!
  
            print("INTERVAL: \(interval_for_procents)")
       print("parametriKredita.procent_stavka_v_den")
            print(parametriKredita.procent_stavka_v_den)
            procents_for_previouse_period =   Double(previouseCreditString.ostatok_po_dolgy * parametriKredita.procent_stavka_v_den * Double(interval_for_procents)).rounded(toPlaces: 2)
            
             print("procents_for_previouse_period: \(procents_for_previouse_period)")
              osnovnoy_dolg = Double(amount - procents_for_previouse_period).rounded(toPlaces: 2)
            ostatok_kredita = Double(previouseCreditString.ostatok_po_dolgy - osnovnoy_dolg).rounded(toPlaces: 2)
            
            
            //5811.72 *  0.00975 * 19  
            
            
            //5811.72       19 dney
            
          
            oplata_procentov = procents_for_previouse_period
           
            
         
         
            
            
            
            
        } else {
            // если первый платеж
            interval_for_procents = 0
            procents_for_previouse_period = 0
            
            osnovnoy_dolg = Double(amount).rounded(toPlaces: 2)
            oplata_procentov = procents_for_previouse_period
            
            
            
            ostatok_kredita = Double(creditData.credit  - osnovnoy_dolg).rounded(toPlaces: 2)
        }
        
      

        
        //проверка есть ли в этот день запись уже
        //добавить проверку если досрочный платеж совпадает с датой официального платежа, значит не считаем разницу со следующей датой!
        
        predicate = NSPredicate(format: "payment_date == %@", date as! NSDate)
        request.predicate = predicate
        
        
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                                              managedObjectContext: coreDataManager.mainManagedObjectContext,
                                                              sectionNameKeyPath: nil,
                                                              cacheName: nil)
        print("check if was string at this date")
        do {
            print("found string at this date already")
            try fetchedResultsController.performFetch()
            
            
        } catch {
            print("Unable to Perform Fetch Request")
            print("\(error), \(error.localizedDescription)")
        }
       
        var localAmount = amount
        var localOsnovnoyDolg = osnovnoy_dolg
        var localostatok_kredita = ostatok_kredita
        
        if let creditStringatThisDate = fetchedResultsController?.fetchedObjects?.last {
            print("FOUNDDDDD")
           print(localAmount)
            if(creditStringatThisDate.osnovnoy_dolg == 0){
                print(localostatok_kredita)
                localOsnovnoyDolg = amount
                localostatok_kredita -= creditStringatThisDate.procent
            }
            
            
            print(creditStringatThisDate.payment_date)
            localAmount += creditStringatThisDate.summ_for_pay
           
            
            
            
            fetchedResultsController.managedObjectContext.delete(creditStringatThisDate)
            
            
            do{
                try fetchedResultsController.managedObjectContext.save()
            } catch{
                fatalError()
            }
            
            
            
            
            
        }
        
        
        
        
        
    
     print("ADD ONE STRING  \(date) \(osnovnoy_dolg)")
        addOneCreditStringtoCoreDataContext(dateOplati: date, osnovnoyDolg: localOsnovnoyDolg, procent: oplata_procentov, summ_for_pay: localAmount, ostatok_kredita: localostatok_kredita)
        
        
  //очистить с даты следующего штатного платежа/
        
       print("CLEAR TABLE FROM DATE \(date)")
         coreDataManager.clearTableFrom(date: date)
        
          // Меняем количество денег которые надо оплатить по графику
        print("NEXT PAYMENT")
       
        
      
       
       
        
        
        
          let requestNew: NSFetchRequest<SberEntity> = SberEntity.fetchRequest()
        requestNew.sortDescriptors = [sort]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: requestNew,
                                                              managedObjectContext: coreDataManager.mainManagedObjectContext,
                                                              sectionNameKeyPath: nil,
                                                              cacheName: nil)
        print("TRY TO FETCH")
        do {
            print("fetch")
            try fetchedResultsController.performFetch()
            
            
        } catch {
            print("Unable to Perform Fetch Request")
            print("\(error), \(error.localizedDescription)")
        }
        
        
        
        
        //////////////
        let sberEntityArray = fetchedResultsController.fetchedObjects
        // Меняем количество денег которые надо оплатить по графику
        
        print("NOEW")
        print(sberEntityArray?.count)
       // for entity in sberEntityArray!{
         //   print(entity.payment_date,entity.osnovnoy_dolg)
        //}
        //print("END PRINT")
        
        
        //берем текущее значение и предыдущее, то есть наш досрочный платеж и ближайший по графику
        if let dosrhochniyPlatesh = sberEntityArray?[(sberEntityArray?.count)!-2],
            let nextPlateshPoGraphicy = sberEntityArray?[(sberEntityArray?.count)!-1]
            {
          
                print("PREVIOUSE VALUE: \(dosrhochniyPlatesh.payment_date) \(dosrhochniyPlatesh.summ_for_pay)")
                  print("NEXT VALUE: \(nextPlateshPoGraphicy.payment_date) \(nextPlateshPoGraphicy.summ_for_pay)")

                
           
                
                    var rangeForProcentPay = Calendar.current.dateComponents([.day], from: dosrhochniyPlatesh.payment_date!, to: nextPlateshPoGraphicy.payment_date!)
                    
                    var rangeInt = rangeForProcentPay.day
                    rangeInt  = rangeInt!
                    print(" Interval: \(rangeInt)")
                
                    var procent_stavka_v_den = creditData.procent/365/100
                    var ostatok:Double = 0
                    
                    if (rangeInt == 0){
                        ostatok = creditData.credit
                    } else {
                        ostatok = dosrhochniyPlatesh.ostatok_po_dolgy
                    }
                
                    print(" ostatok: \(ostatok)")
                    var procents_for_previouse_period =   Double(ostatok * procent_stavka_v_den * Double(rangeInt!)).rounded(toPlaces: 2)
                    
                    
                    
                    print(" procent_stavka_v_den: \(procent_stavka_v_den)")
                    print(" procents_for_previouse_period: \(procents_for_previouse_period)")
                    nextPlateshPoGraphicy.procent = procents_for_previouse_period
                
                //currentValue.summ_for_pay = procents_for_previouse_period
                
            
                
                
                /*
                if ( previouseValue.summ_for_pay > currentValue.summ_for_pay){
                    currentValue.summ_for_pay = previouseValue.summ_for_pay - currentValue.summ_for_pay
                    currentValue.osnovnoy_dolg = 0
                    currentValue.ostatok_po_dolgy = previouseValue.ostatok_po_dolgy
                } else {
                    currentValue.summ_for_pay = procents_for_previouse_period
                }
                
                    currentValue.ostatok_po_dolgy = currentValue.ostatok_po_dolgy - (currentValue.summ_for_pay - procents_for_previouse_period)
                */
                
                
                //если мы платим столько что покрываем процент
              
                print("DOSROCHNIY PLATESH")
                  print("dosrhochniyPlatesh.summ_for_pay \(dosrhochniyPlatesh.summ_for_pay)")
                  print("nextPlateshPoGraphicy.summ_for_pay \(nextPlateshPoGraphicy.summ_for_pay)")
                  print("DOSROCHNIY PLATESH")
               
                /*
 
                 //последний процент считается так, уможноам ставку в день  0.00032054794520547944 на количество дней с 3 августа по 22 августа то етсь 19 и умножаем на 5811.72 остаток кредита -  в основной долг заносим всю сумму и получаем сумму к оплате
                 var lastDatePoDogovory = Calendar.current.date(from: creditData.dataLastKredita)
                 paymentDatePoGraphicy = Calendar.current.date(byAdding: .month, value: -1, to: paymentDatePoGraphicy!)
                 var rangeForProcentPay = Calendar.current.dateComponents([.day], from: lastDatePoDogovory!, to: paymentDatePoGraphicy!)
                 
                 var rangeInt = rangeForProcentPay.day
                 rangeInt  = rangeInt!
                 print(" Interval: \(rangeInt)")
                 
                 print("parametriKredita.procent_stavka_v_den \(parametriKredita.procent_stavka_v_den)")
                 print("range \(Double(rangeInt!))")
                 print("ostatok \(ostatok_kredita)")
                 var lastProcent = parametriKredita.procent_stavka_v_den * Double(rangeInt!) * ostatok_kredita_final_day
                 addOneCreditStringtoCoreDataContext(dateOplati: lastDatePoDogovory!, osnovnoyDolg: ostatok_kredita_final_day, procent: lastProcent, summ_for_pay: ostatok_kredita_final_day+lastProcent, ostatok_kredita: ostatok_kredita)
 
 nextPlateshPoGraphicy.procent = parametriKredita.procent_stavka_v_den
 
 */
                
                
                
                //OSTALSA ETOT KUSOK!!! 20.06.19
                if(dosrhochniyPlatesh.summ_for_pay >= nextPlateshPoGraphicy.summ_for_pay){
                
        
                    
                    nextPlateshPoGraphicy.osnovnoy_dolg = 0
                   nextPlateshPoGraphicy.summ_for_pay = nextPlateshPoGraphicy.procent
                    
                    //nextPlateshPoGraphicy.summ_for_pay = dosrhochniyPlatesh.summ_for_pay
                    nextPlateshPoGraphicy.ostatok_po_dolgy = dosrhochniyPlatesh.ostatok_po_dolgy
                }
                
                if(dosrhochniyPlatesh.summ_for_pay<nextPlateshPoGraphicy.summ_for_pay){
                    nextPlateshPoGraphicy.summ_for_pay -= dosrhochniyPlatesh.summ_for_pay
                    
                }
                
                
                 //nextPlateshPoGraphicy.osnovnoy_dolg = 0
               // nextPlateshPoGraphicy.summ_for_pay = nextPlateshPoGraphicy.procent
             
                nextPlateshPoGraphicy.ostatok_po_dolgy = dosrhochniyPlatesh.ostatok_po_dolgy
                
                print("NEXT PLATESH PRO GRAPHICY")
                print(nextPlateshPoGraphicy.summ_for_pay)
                
               
                var creditDataNew = creditData
                    print("NEW CREDIT PARAMETERS")
                
                
                      creditDataNew.credit = nextPlateshPoGraphicy.ostatok_po_dolgy
                    print("nextPlateshPoGraphicy.ostatok_po_dolgy \(nextPlateshPoGraphicy.ostatok_po_dolgy)")
                   
                     var  nextMonth = nextPlateshPoGraphicy.payment_date
                        print("BEFORE NEXT MONTH: \(nextMonth)")
                    nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: nextMonth!)
                    print("NEXT MONTH: \(nextMonth)")
                   // creditDataNew.credit = ostatok_kredita
                    creditDataNew.dataPlatesha = Calendar.current.dateComponents([.month,.day,.year],from:nextMonth!)
                  //////////
                    
                    var newRangeForPay = Calendar.current.dateComponents([.month], from: creditDataNew.dataVidachiKredita, to: creditDataNew.dataPlatesha)
                    print("00! \(newRangeForPay)")
                    
                    var newRangeForPayInt = newRangeForPay.month
                    print("!!!!!!")
                    print(newRangeForPayInt)
                  
                    creditDataNew.range = Double(Int(creditDataNew.range) - newRangeForPayInt!)
                
                //разобраться с этим все зависит от первого платежа, если он не совпадает
                creditDataNew.range+=1
                    
                    ////////////////
                //var newPayment = Calendar.current.date(byAdding: .month, value: -1, to: creditDataNew.dataPlatesha)
              
                
            
                   print(creditDataNew.credit)
                print("NOVIY CREDIT \(creditDataNew.dataPlatesha)")
                  print("NOVIY CREDIT ostatok \(creditDataNew.credit)")
                   print("NOVIY CREDIT data \(creditDataNew.dataPlatesha)")
                
                
                    calculateParametriKredita(creditData:creditDataNew)
                    
                    
                    
                    
                  calculateKredit(creditData: creditDataNew)
                
                } else{
                   print("HERE ALEX")
                }
                
                
            
       // } else{
       //       print("HERE ALEX2")
       // }
        
        
        
        
        
        
        
        
        
        
      
        
        
        //////////////////
        
        
        
       
        
        
        
        
        
        
        
        
        
        
        
        
       
    
        
        
        
        
    }
    
    func getNextDateForPaymentFromThisDate(date:Date){
      
        
    }
    
    
    
    
    
    
    
    func addOneCreditStringtoCoreDataContext(dateOplati:Date,osnovnoyDolg:Double,procent:Double,summ_for_pay:Double, ostatok_kredita:Double){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        
        
        if let entityDescription = NSEntityDescription.entity(forEntityName: "SberEntity", in: coreDataManager.mainManagedObjectContext){
        //    print(entityDescription.name ?? "No Name")
        //    print(entityDescription.properties)
            
            let entity = SberEntity(context: coreDataManager.mainManagedObjectContext)
            
            entity.payment_date = dateOplati
          //  print("PAYMENT")
         //   print(dateOplati)
            entity.osnovnoy_dolg =  Double(osnovnoyDolg).rounded(toPlaces: 2)
            entity.procent = Double(procent).rounded(toPlaces: 2)
            entity.summ_for_pay = Double(summ_for_pay).rounded(toPlaces: 2)
            entity.ostatok_po_dolgy = Double(ostatok_kredita).rounded(toPlaces: 2)
           // print("entity was added")
        }
    }
    
    
    
    
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

precedencegroup PowerPrecedence { higherThan: MultiplicationPrecedence }
infix operator ^^ : PowerPrecedence
func ^^ (radix: Double, power: Double) -> Double {
    return Double(pow(Double(radix), Double(power)))
}
