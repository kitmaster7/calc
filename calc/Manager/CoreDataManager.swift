//
//  CoreDataManager.swift
//  Notes
//
//  Created by Aleksey Shitikov 2019
//
//

import CoreData
import UIKit
final class CoreDataManager {
    
    // MARK: - Properties
    
    private let modelName: String
   
    // MARK: - Initialization
    
    init(modelName: String) {
        self.modelName = modelName
        
        setupNotificationHandling()
    }
    
    private lazy var managedObjectModel: NSManagedObjectModel? = {
        // Fetch Model URL
        guard let modelURL = Bundle.main.url(forResource: self.modelName, withExtension: "momd") else {
            fatalError("Unable to Find Data Model")
        }
        
        // Initialize Managed Object Model
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Unable to Load Data Model")
        }
        
        return managedObjectModel
    }()
    
    
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {

        guard let managedObjectModel = self.managedObjectModel else {
            return nil
        }

        
        
        
        // Initialize Persistent Store Coordinator
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        // Helpers
        let fileManager = FileManager.default
        let storeName = "\(self.modelName).sqlite"
        
        // URL Documents Directory
        let documentsDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        // URL Persistent Store
        let persistentStoreURL = documentsDirectoryURL.appendingPathComponent(storeName)
        
        do {
            let options = [
                NSMigratePersistentStoresAutomaticallyOption : true,
                NSInferMappingModelAutomaticallyOption : true
            ]
            
            // Add Persistent Store
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                              configurationName: nil,
                                                              at: persistentStoreURL,
                                                              options: options)
            
        } catch {
            fatalError("Unable to Add Persistent Store")
        }
        
        return persistentStoreCoordinator
    }()
    
    
    
    
    // MARK: -
    //только set будет приватным, остальные - get публичным.
    private(set) lazy var mainManagedObjectContext: NSManagedObjectContext = {
        // Initialize Managed Object Context
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        
        // Configure Managed Object Context
        managedObjectContext.parent = self.privateManagedObjectContext
        
        return managedObjectContext
    }()
    
    private lazy var privateManagedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        
        return managedObjectContext
    }()
    
    
   
    func clearTable(){
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "SberEntity")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do{
            try mainManagedObjectContext.execute(request)
        }catch{
            print("Error Clearing Transaction Table")
        }
    }
    
    
    func clearTableFrom(date: Date){
      
        let request: NSFetchRequest<SberEntity> = SberEntity.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(SberEntity.payment_date), ascending: true)
        request.sortDescriptors = [sort]
        
        
        let predicate = NSPredicate(format: "payment_date > %@", date as! NSDate)
        request.predicate = predicate
        
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                                                  managedObjectContext: self.mainManagedObjectContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        
        
        do{
            try fetchedResultsController.performFetch()
        } catch{
            fatalError("DFSDF")
        }
        
        let items = fetchedResultsController.fetchedObjects
        print("!!!!!!")
        print(items)
       var firstValue = true
        for item in items!{
            if(!firstValue){
            fetchedResultsController.managedObjectContext.delete(item)
            }
           firstValue = false
        }
        do{
        try fetchedResultsController.managedObjectContext.save()
        } catch{
            fatalError()
        }
        
        
        print("NEW VALUE")
        print(fetchedResultsController.fetchedObjects?.count)
      
    }
    

    
    
    func deleteLastStringandAddNewOneForLastMonth(){
        
        let request: NSFetchRequest<SberEntity> = SberEntity.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(SberEntity.payment_date), ascending: true)
        request.sortDescriptors = [sort]
        
        
      
        
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: request,
                                                                  managedObjectContext: self.mainManagedObjectContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        
        
        do{
            try fetchedResultsController.performFetch()
        } catch{
            fatalError("DFSDF")
        }
        
        let items = fetchedResultsController.fetchedObjects
        if let lastItem = fetchedResultsController.fetchedObjects?.last{
            print("LAST ITEM \(lastItem)")
            let obj1 = lastItem as! NSManagedObject
            fetchedResultsController.managedObjectContext.delete(obj1)
        }
    
        do{
            try fetchedResultsController.managedObjectContext.save()
        } catch{
            fatalError()
        }
        
      
        
    }
    
    
    
    
        
        
        
     /*
        if let result = try? context.fetch(fetchRequest) {
            for object in result {
                context.delete(object)
            }
        }
        */
        
     
   
   
    
    // MARK: - Notification Handling
    
    @objc func saveChanges(_ notification: Notification) {
        saveChanges()
    }
    
    // MARK: - Helper Methods
    
    private func setupNotificationHandling() {
        let notificationCenter = NotificationCenter.default
    
        
    
        notificationCenter.addObserver(self,
                                       selector: #selector(saveChanges(_:)),
                                       name: UIApplication.willTerminateNotification,
                                       object: nil)
       
        notificationCenter.addObserver(self,
                                       selector: #selector(saveChanges(_:)),
                                       name: UIApplication.didEnterBackgroundNotification,
                                       object: nil)
    }
    
    // MARK: -
    
    private func saveChanges() {
        mainManagedObjectContext.performAndWait {
            do {
                if self.mainManagedObjectContext.hasChanges {
                    try self.mainManagedObjectContext.save()
                }
            } catch {
                print("Unable to Save Changes of Main Managed Object Context")
                print("\(error), \(error.localizedDescription)")
            }
        }
        
        privateManagedObjectContext.perform {
            do {
                if self.privateManagedObjectContext.hasChanges {
                    try self.privateManagedObjectContext.save()
                }
            } catch {
                print("Unable to Save Changes of Private Managed Object Context")
                print("\(error), \(error.localizedDescription)")
            }
        }
    }
    
    
  
    
    
}

